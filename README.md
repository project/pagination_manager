# Pagination Manager

## Contents of this file

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Troubleshooting
 - Maintainers
## Introduction
This module provides a flexible pagination system.
## Requirements

This module requires Drupal core >= ^8.7.7

## Installation

Install as you would normally install a contributed Drupal module. Visit https://www.drupal.org/node/1897420 for further information.

## Maintainers

Current maintainers:
- Ernest KOUASSI ([@ekouassi](https://www.drupal.org/u/ekouassi))
