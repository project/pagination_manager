<?php

namespace Drupal\pagination_manager;

use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Pagination manager filter class.
 */
class Filter extends ParameterBag
{
    private string $property;
    private string $type;
    private bool $searchable;
    private bool $orderable;

    /**
     * @param string $property
     * @param string $type
     * @param bool   $searchable
     * @param bool   $orderable
     */
    public function __construct(
        string $property,
        string $type,
        bool $searchable = false,
        bool $orderable = false
    ) {
        $this->property = $property;
        $this->type = $type;
        $this->searchable = $searchable;
        $this->orderable = $orderable;
    }

    /**
     * @return string
     */
    public function getProperty(): string
    {
        return $this->property;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return bool
     */
    public function isSearchable(): bool
    {
        return $this->searchable;
    }

    /**
     * @return bool
     */
    public function isOrderable(): bool
    {
        return $this->orderable;
    }
}
