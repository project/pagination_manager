<?php

namespace Drupal\pagination_manager\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Module configuration form.
 */
class PaginationManagerConfigForm extends ConfigFormBase
{
    protected function getEditableConfigNames(): array
    {
        return ['pagination_manager.settings'];
    }

    public function getFormId(): string
    {
        return 'pagination_manager_settings';
    }

    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $config = $this->config('pagination_manager.settings');
        $form['page_parameter_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Pagination page parameter name'),
        '#default_value' => $config->get('page_parameter_name', 'page'),
        ];
        $form['pagination_object_parameter'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Pagination collections index parameter name'),
        '#default_value' => $config->get('pagination_object_parameter', 'items'),
        ];
        $form['total_item_parameter_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Pagination total item parameter name'),
        '#default_value' => $config->get('total_item_parameter_name', 'totalItems'),
        ];
        $form['item_per_page_parameter_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Pagination item per page parameter name'),
        '#default_value' => $config->get('item_per_page_parameter_name', 'itemPerPage'),
        ];
        $form['item_per_page'] = [
        '#type' => 'number',
        '#title' => $this->t('Pagination item per page'),
        '#default_value' => $config->get('item_per_page', 10),
        ];
        $form['page_number_parameter_name'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Page number parameter name'),
        '#default_value' => $config->get('page_number_parameter_name', 10),
        ];

        return parent::buildForm($form, $form_state);
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        $form_state->cleanValues();
        $this->config('pagination_manager.settings')
            ->setData($form_state->getValues())
            ->save();

        parent::submitForm($form, $form_state);
    }
}
