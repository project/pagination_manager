<?php

namespace Drupal\pagination_manager\Exception;

/**
 * Sorted not available exception.
 */
class SortAttributeNotAvailableException extends PaginationException
{
    private array $sort;

    /**
     * @param array       $sort
     * @param string|null $message
     * @param int|null    $code
     * @param mixed|NULL  $previous
     */
    public function __construct(array $sort, ?string $message = null, ?int $code = null, mixed $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->sort = $sort;
    }

    /**
     * @return array
     */
    public function getSort(): array
    {
        return $this->sort;
    }
}
