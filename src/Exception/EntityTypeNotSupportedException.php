<?php

namespace Drupal\pagination_manager\Exception;


/**
 * Entity type not supported exception.
 */
class EntityTypeNotSupportedException extends PaginationException
{

}
