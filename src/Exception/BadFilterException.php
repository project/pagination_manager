<?php

namespace Drupal\pagination_manager\Exception;


/**
 * Bad filter exception.
 */
class BadFilterException extends PaginationException
{

}
