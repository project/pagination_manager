<?php

namespace Drupal\pagination_manager\Exception;

/**
 * Pagination failure exception.
 */
class PaginationException extends \RuntimeException
{

}
