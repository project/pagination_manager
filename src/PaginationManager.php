<?php

namespace Drupal\pagination_manager;

use Symfony\Component\HttpFoundation\Request;

/**
 * Core pagination manager class
 */
class PaginationManager
{
    private Configuration $configuration;

    /**
     * @param Configuration $configuration
     */
    public function __construct(Configuration $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param  string $entityTypeId
     * @return array
     */
    public function getConfiguration(string $entityTypeId): array
    {
        if (true === $this->configuration->has($entityTypeId)) {
            return $this->configuration->get($entityTypeId);
        }

        throw new \InvalidArgumentException(sprintf('The entity type with ID "%s" configuration name is not attached to a pagination manager.', $entityTypeId));
    }

    public function paginate(string $entityTypeId, Request $request, array $criteria = [], array $orderBy = []): Page
    {
        return $this->createQuery($entityTypeId, $request, $criteria, $orderBy)->execute();
    }

    private function createQuery(string $entityTypeId, Request $request, array $criteria = [], array $orderBy = []): Query
    {
        $configuration = $this->getConfiguration($entityTypeId);
        $mappings = $configuration['mappings'];
        $filters = $mappings['fields'];
        $sort = $mappings['sort'];
        $itemPerPage = $configuration['item_per_page'];
        $page = $configuration['page'];
        $paginated = $configuration['paginated'];
        $delimiter = $mappings['delimiter'];

        if (null !== $sort) {
            $sortProperty = array_keys($sort['order']);
            $sortType = array_values($sort['order']);
        }

        foreach ($filters as $key => $filter) {
            if (true === $filter['searchable'] && null !== ($value = $request->query->get($filter['name'] ?? $key))) {
                if (true === is_array($value)  && true === empty($value[array_key_first($value)])) {
                    continue;
                } else {
                    $criteria[$key] = $this->queryToArray($value, $filter, $delimiter);
                }
            }
            if (true === $filter['orderable'] && null !== $sort) {
                $existSort = in_array($key, $sortProperty, true);

                if (false === $existSort && false === isset($sort['order'][$key])) {
                    continue;
                }
                if (true === $existSort) {
                    $sort[$key] = true === in_array($key, $sortType, true) ? 'DESC' : 'ASC';
                } else {
                    $sort[$key] = true === in_array($key, $sortProperty, true) ? 'DESC' : $sort['order'][$key];
                }
            }
        }

        return new Query(
            \Drupal::entityTypeManager(),
            $entityTypeId,
            $request->query->getInt($configuration['item_per_page_parameter_name'], $itemPerPage ?? 10),
            $request->query->getInt($configuration['page_parameter_name'], $page ?? '1'),
            $request->query->getBoolean('paginated', $paginated ?? true),
            $filters ?? [],
            $configuration,
            $criteria,
            $orderBy,
        );
    }

    /**
     * @param  mixed       $value
     * @param  array       $filter
     * @param  string|null $delimiter
     * @return array
     */
    protected function queryToArray(mixed $value, array $filter, ?string $delimiter = null): mixed
    {
        if (null !== $delimiter && true === is_array($value)) {
            $key = array_key_first($value);

            $data = explode($delimiter, $value[$key]);

            if (1 < count($data)) {
                $dataSet = [];
                foreach ($data as $datum) {
                    $dataSet[$key][] = static::castTo($datum, $filter['type']);
                }

                return $dataSet;
            }

            return [$key => static::castTo($data[0], $filter['type'])];
        }
        if ('datetime' === $filter['type'] && false === is_array($value)) {
            return ['BETWEEN' => [
            static::castTo($value.' 00:00:00', $filter['type']),
            static::castTo($value.' 23:59:59', $filter['type'])
            ]];
        }

        return static::castTo($value, $filter['type']);
    }

    public static function castTo(string $value, string $type)
    {
        if ('datetime' === $type) {
            return strtotime($value);
        }
        if ('bool' === $type || 'boolean' === $type) {
            return !('false' === $value || '0' === $value);
        }
        if (false === settype($value, $type)) {
            return;
        }

        return $value;
    }
}
