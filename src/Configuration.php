<?php

namespace Drupal\pagination_manager;

use Drupal\Component\Plugin\Exception\InvalidDeriverException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDerivativeDiscoveryDecorator;
use Drupal\Core\Plugin\Discovery\YamlDiscovery;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Pagination manager configuration.
 */
class Configuration extends ParameterBag
{
    private ConfigFactoryInterface $configFactory;
    private Filter $filter;

    /**
     * @param ConfigFactoryInterface $configFactory
     */
    public function __construct(ConfigFactoryInterface $configFactory)
    {
        $this->configFactory = $configFactory;
        $config = $this->configFactory->get('pagination_manager.settings');

        foreach ($this->getDefinitions() as $key => $definition) {
            $this->set(
                $key, [
                'twig' => $config->get('twig'),
                'paginated' => (bool)$config->get() ?? true,
                'page' => 1,
                'page_parameter_name' => $config->get('page_parameter_name'),
                'page_number_parameter_name' => $config->get('page_number_parameter_name'),
                'item_per_page' => (int)$config->get('item_per_page'),
                'item_per_page_parameter_name' => $config->get('item_per_page_parameter_name'),
                'total_items' => 0,
                'total_item_parameter_name' => $config->get('total_item_parameter_name'),
                'pagination_object_parameter' => $config->get('pagination_object_parameter'),
                'mappings' => $definition,
                $this->createFilter($definition['filters'] ?? [])
                ]
            );
        }
    }

    /**
     * @return ConfigFactoryInterface
     */
    public function getConfigFactory(): ConfigFactoryInterface
    {
        return $this->configFactory;
    }

    protected function createFilter(array $filters): self
    {
        if (true === reset($filters)) {
            return $this;
        }

        foreach ($filters as $key => $filter) {
            if (true === isset($mappings['type'])) {
                $this->filter = new Filter($key, $mappings['type'], $mappings['searchable'], $mappings['orderable']);
                $this->set($key, $this->filter);
            }
        }

        return $this;
    }

    public function getFilter(): Filter
    {
        return $this->filter;
    }

    /**
     * @throws InvalidDeriverException
     */
    protected function getDefinitions()
    {
        /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler 
*/
        $moduleHandler = \Drupal::moduleHandler();
        $YAMLDiscovery = new YamlDiscovery('pagination', $moduleHandler->getModuleDirectories());

        return (new ContainerDerivativeDiscoveryDecorator($YAMLDiscovery))->getDefinitions();
    }
}
