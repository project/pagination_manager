<?php

namespace Drupal\pagination_manager;

/**
 * Page manager class
 */
class Page
{
    private int $page;
    private int $itemPerPage;
    private int $totalItems;
    private array $filters;
    private array $orderBy;
    private array $items;
    private int $pageNumber;
    private array $configuration;

    /**
     * @param int   $page
     * @param int   $itemPerPage
     * @param int   $totalItems
     * @param array $filters
     * @param array $orderBy
     * @param int   $pageNumber
     * @param array $items
     * @param array $configuration
     */
    public function __construct(
        int   $page,
        int   $itemPerPage,
        int   $totalItems,
        array $filters,
        array $orderBy,
        int   $pageNumber,
        array $items,
        array $configuration
    ) {
        $this->page = $page;
        $this->itemPerPage = $itemPerPage;
        $this->totalItems = $totalItems;
        $this->filters = $filters;
        $this->orderBy = $orderBy;
        $this->items = $items;
        $this->configuration = $configuration;

        if (0 < $pageNumber) {
            $this->pageNumber = $pageNumber;
        } else {
            $this->pageNumber = 1;
            $items = $this->totalItems - $this->itemPerPage;

            while ($items > 0) {
                $items -= $this->itemPerPage;
                ++$this->pageNumber;
            }
        }
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @return int
     */
    public function getItemPerPage(): int
    {
        return $this->itemPerPage;
    }

    public function getTotalItems(): int
    {
        return $this->totalItems;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @return array
     */
    public function getOrderBy(): array
    {
        return $this->orderBy;
    }

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @return int
     */
    public function getPageNumber(): int
    {
        return $this->pageNumber;
    }

    public function toArray(array $excludedFields = []): array
    {
        $data = [
        $this->configuration['page_parameter_name'] => $this->page,
        $this->configuration['item_per_page_parameter_name'] => $this->itemPerPage,
        $this->configuration['total_item_parameter_name'] => $this->totalItems,
        $this->configuration['page_number_parameter_name'] => $this->pageNumber,
        $this->configuration['pagination_object_parameter'] => $this->items
        ];

        if (false === empty($excludedFields)) {
            foreach ($excludedFields as $field) {
                if (true === isset($data[$field])) {
                    unset($data[$field]);
                }
            }
        }

        return $data;
    }
}
