<?php

namespace Drupal\pagination_manager;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Pager\Pager;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Core query execution class
 */
class Query
{
    public const OPERATORS = [
    '=',
    '<>',
    '>',
    '>=',
    '<',
    '<=',
    'STARTS_WITH',
    'CONTAINS',
    'ENDS_WITH',
    'IN',
    'NOT IN',
    'IS NULL',
    'IS NOT NULL',
    'BETWEEN',
    'NOT BETWEEN'
    ];

    private EntityTypeManagerInterface $entityTypeManager;
    private string $entityTypeId;
    private int $itemPerPage;
    private int $page;
    private bool $paginated;
    private array $filters;
    private array $configuration;
    private array $orderBy;
    private array $criteria;

    /**
     * @param EntityTypeManagerInterface $entityTypeManager
     * @param string                     $entityTypeId
     * @param int                        $itemPerPage
     * @param int                        $page
     * @param bool                       $paginated
     * @param array                      $filters
     * @param array                      $configuration
     * @param array                      $criteria
     * @param array                      $orderBy
     */
    public function __construct(
        EntityTypeManagerInterface $entityTypeManager,
        string $entityTypeId,
        int $itemPerPage,
        int $page,
        bool $paginated,
        array $filters,
        array $configuration,
        array $criteria = [],
        array $orderBy = []
    ) {
        if (1 > $itemPerPage) {
            throw new \LogicException(sprintf('The number of items per page must be greater than 0, "%s" given.', $itemPerPage));
        }

        $this->entityTypeManager = $entityTypeManager;
        $this->entityTypeId = $entityTypeId;
        $this->itemPerPage = $itemPerPage;
        $this->page = $page;
        $this->paginated = $paginated;
        $this->filters = $filters;
        $this->configuration = $configuration;
        $this->criteria = $criteria;
        $this->orderBy = $orderBy;
    }

    /**
     * @return EntityTypeManagerInterface
     */
    public function getEntityTypeManager(): EntityTypeManagerInterface
    {
        return $this->entityTypeManager;
    }

    /**
     * @return string
     */
    public function getEntityTypeId(): string
    {
        return $this->entityTypeId;
    }

    /**
     * @return int
     */
    public function getItemPerPage(): int
    {
        return $this->itemPerPage;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        if (false === $this->paginated) {
            return 1;
        }

        return $this->page;
    }

    /**
     * @return bool
     */
    public function isPaginated(): bool
    {
        return $this->paginated;
    }

    /**
     * @return Page
     * @throws InvalidPluginDefinitionException
     * @throws PluginNotFoundException
     */
    public function execute(): Page
    {
        if (1 > $this->page) {
            throw new BadRequestHttpException(t(sprintf('Minimum page number must be 1 but %s given', $this->getPage())));
        }

        $storage = $this->entityTypeManager
            ->getStorage($this->entityTypeId);
        $query = $storage->getQuery();

        if (0 < count($this->criteria)) {
            foreach ($this->criteria as $key => $value) {
                if (false === is_array($value)) {
                    $query->condition($key, $value);
                }
                if (true === is_array($value) && true === in_array(strtoupper(array_key_first($value)), static::OPERATORS)) {
                    $query->condition($key, $value[array_key_first($value)], strtoupper(array_key_first($value)));
                }
            }
        }
        if (false !== reset($this->orderBy)) {
            $key = array_key_first($this->orderBy);
            $query->sort($key, $this->orderBy[$key]);
        }

        $fakeQuery = clone $query;
        $totalItems = $fakeQuery->count()->execute();

        if (true === $this->isPaginated()) {
            $pager = new Pager((int)$totalItems, $this->getItemPerPage(), $this->getPage() - 1);
            $ids = $query->range(0 === $pager->getCurrentPage() ? 0 : $pager->getLimit() * $pager->getCurrentPage(), $pager->getLimit())->execute();
        } else {
            $ids = $query->execute();
        }

        $items =  $storage->loadMultiple(array_values($ids));

        return new Page(
            $this->getPage(),
            true === $this->paginated ? $this->getItemPerPage() : $totalItems,
            $totalItems,
            $this->filters,
            $this->orderBy,
            true === $this->paginated ? ceil($totalItems / $this->getItemPerPage()) : 1,
            array_values($items),
            $this->configuration
        );
    }
}
